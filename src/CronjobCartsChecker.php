<?php

namespace Drupal\commerce_quickpay_gateway;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_quickpay_gateway\Exceptions\ActiveGatewayNotFound;
use Drupal\commerce_quickpay_gateway\Traits\OrderIDGeneratorTrait;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

class CronjobCartsChecker {
  use OrderIDGeneratorTrait;

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Logger interface.
   *
   * @var LoggerInterface
   */
  protected $logger;

  public function __construct(EntityTypeManager $entityTypeManager, LoggerInterface $logger)
  {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
  }

  public function checkCarts() {
    /** @var Order[] $orders */
    $orders = Order::loadMultiple();

    $completed = 0;

    $draftOrders = array_filter($orders, function (Order $order) {
      /** @var StateItemInterface $state */
      $state = $order->getState();
      return $state->getId() === 'draft';
    });

    foreach ($draftOrders as $draftOrder) {
      $this->logger->info($draftOrder->id());
      try {
        $paymentDetails = $this->fetchPayment($draftOrder);
      } catch (ActiveGatewayNotFound $e) {
        $this->logger->warning("Couldn't check payment for carts, since no active payment gateway was found");
        return false;
      }

      if ($paymentDetails) {
        $this->completeOrder($draftOrder, $paymentDetails);
        $completed++;
      }
    }

    $this->logger->info(sprintf("Validated %s drafts, marked %s as completed", count($draftOrders), $completed));
  }

  /**
   * Get the current active gateway.
   *
   * @return PaymentGateway|null
   */
  private function getActiveGateway()
  {
    /** @var PaymentGateway[] $gateways */
    $gateways = PaymentGateway::loadMultiple();

    foreach ($gateways as $gateway) {
      if ($gateway->status()) {
        return $gateway;
      }
    }

    return null;
  }

  /**
   * Fetch information about a payment from quickpay.
   *
   * @param Order $order
   *
   * @return \stdClass|false
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function fetchPayment(Order $order) {
    $gateway = $this->getActiveGateway();

    if (!$gateway) {
      throw new ActiveGatewayNotFound();
    }

    $order_id = $this->generateOrderId($order->id(), $gateway->getPluginConfiguration()['order_prefix']);

    $client = \Drupal::httpClient();
    $response = $client->request('GET', "https://api.quickpay.net/payments?order_id={$order_id}", [
      RequestOptions::HEADERS => [
        'content-type' => 'application/json',
        'Accept-Version' => 'v10',
        'Authorization' => sprintf('Basic %s', base64_encode(":{$gateway->getPluginConfiguration()['api_key']}")),
      ],
    ]);
    $paymentDetails = json_decode($response->getBody()->getContents());

    return isset($paymentDetails[0]->accepted) && $paymentDetails[0]->accepted ? $paymentDetails[0] : false;
  }

  /**
   * Mark an order as completed.
   *
   * @param Order $order
   * @param \stdClass $paymentDetails
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function completeOrder(Order $order, $paymentDetails)
  {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'Accepted',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $paymentDetails->variables->payment_gateway,
      'order_id' => $order->id(),
      'remote_id' => $paymentDetails->id,
      'remote_state' =>  $this->getRemoteState($paymentDetails),
    ]);

    $payment->save();

    $order->getState()->applyTransitionById('place');

    $order->save();

    \Drupal::logger('commerce_quickpay_gateway')
      ->notice(sprintf("Order with id \"%s\" was marked as completed by CRON", $order->id()));
  }

  /**
   * Get the state from the transaction.
   *
   * @param object $content
   *   The request data from QuickPay.
   *
   * @return string
   */
  private function getRemoteState($content) {
    $latest_operation = end($content->operations);
    return $latest_operation->qp_status_msg;
  }
}
