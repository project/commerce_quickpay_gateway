<?php

namespace Drupal\commerce_quickpay_gateway\Traits;

trait OrderIDGeneratorTrait {
  /**
   * Generate a valid Order ID for Quickpay.
   *
   * @param $order_id
   * @param string $order_prefix
   *
   * @return string
   */
  public function generateOrderId($order_id, $order_prefix = '') {
    // Ensure that Order number is at least 4 characters otherwise QuickPay will reject the request.
    if (strlen($order_id) < 4) {
      $order_id = substr('000' . $order_id, -4);
    }

    if (!empty($order_prefix)) {
      $order_id = $order_prefix . $order_id;
    }

    return $order_id;
  }
}
