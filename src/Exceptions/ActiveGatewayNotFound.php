<?php

namespace Drupal\commerce_quickpay_gateway\Exceptions;

class ActiveGatewayNotFound extends \Exception { }
